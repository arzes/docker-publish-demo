#!/bin/bash

# 参数：
# $1 镜像
# $2 映射port
# $3 taskid
mkdir /opt/publishwork/${3}/dockerlog
extp=${2}
extp1=${extp//\"/}
extp2=${extp1//\"/}
docker run -d -v /opt/publishwork/${3}/dockerlog:/opt/data/logs ${extp2} ${1}
echo '<end></end>'
exit 0