#!/bin/bash

# 参数：
# $1 进程UUID
# $2 publish存放项目目录
# $3 用户名

cd ${2}/${1}/dockertask
docker build -t ${3}-${1} ${2}/${1}/dockertask
echo '<publishfinish></publishfinish>'
echo '<end></end>'
exit 0