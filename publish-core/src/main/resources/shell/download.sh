#!/bin/bash

# 参数：
# $1 进程UUID
# $2 publish存放项目目录
# $3 git url
# $4 branch
# $5 项目主路径

cd ${2}/${1}
git clone $3 -b $4
echo '<end></end>'
exit 0