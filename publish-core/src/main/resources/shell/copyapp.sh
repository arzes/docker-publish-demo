#!/bin/bash

# 参数：
# $1 进程UUID
# $2 publish存放项目目录
# $3 app 目录
cd ${2}/${1}/dockertask
cp ${3} ${2}/${1}/dockertask
echo '<end></end>'
exit 0