#!/bin/bash

# 参数：
# $1 进程UUID
# $2 publish存放项目目录
# $3 项目主路径
# $4 app目录

cd ${2}/${1}/${3}/${4}
mvn clean install -Dmaven.test.skip=true -U
echo '<end></end>'
exit 0