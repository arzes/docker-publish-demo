#!/bin/bash

# 参数：
# $1 进程UUID
# $2 publish存放项目目录
# $3 jdk 目录
# $4 zookeeper 目录
cd ${2}
mkdir ${1}
cd ${1}
mkdir dockertask
cd dockertask
cp -r ${3} ${2}/${1}/dockertask
cp -r ${4} ${2}/${1}/dockertask
echo '<end></end>'
exit 0