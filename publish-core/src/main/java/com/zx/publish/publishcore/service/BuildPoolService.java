package com.zx.publish.publishcore.service;

import com.zx.publish.publishcore.config.DockerConf;
import com.zx.publish.publishcore.config.PublishConf;
import com.zx.publish.publishcore.mapper.AppModelMappe;
import com.zx.publish.publishcore.mapper.ProjectModelMapper;
import com.zx.publish.publishcore.mapper.PublishLogModelMapper;
import com.zx.publish.publishcore.mapper.PublishTaskMapper;
import com.zx.publish.publishcore.model.AppModel;
import com.zx.publish.publishcore.model.ProjectModel;
import com.zx.publish.publishcore.util.ShellUtil;
import com.zx.publish.publishcoreapi.BuildTaskService;
import com.zx.publish.publishcoreapi.enums.PublishTaskStatusEnum;
import com.zx.publish.publishcoreapi.params.BuildAppParam;
import com.zx.publish.publishcoreapi.vo.AppModelVo;
import com.zx.publish.publishcoreapi.vo.ProjectModelVo;
import com.zx.publish.publishcoreapi.vo.PublishTaskModelVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class BuildPoolService {
    @Autowired
    private PublishConf publishConf;
    @Autowired
    private DockerConf dockerConf;

    @Autowired
    private ShellUtil shellUtil;
    @Autowired
    private PublishLogModelMapper publishLogModelMapper;
    @Autowired
    private BuildTaskService buildTaskService;
    @Autowired
    private ProjectModelMapper projectModelMapper;
    @Autowired
    private AppModelMappe appModelMappe;
    @Autowired
    private PublishTaskMapper publishTaskMapper;

    private void createDockerFile(String taskId, ProjectModelVo projectModelVo, List<AppModelVo> appModelVos) throws Exception {
        //动态创建Dockerfile
        File dockerFile = new File(publishConf.getWorkspace() + File.separator + taskId + File.separator + "dockertask" + File.separator + "Dockerfile");
        StringBuilder builder = new StringBuilder();
        builder.append("FROM " + dockerConf.getImgUbuntu() + " \n");
        builder.append("MAINTAINER zhouxin \n");
        builder.append("COPY " + publishConf.getJdkDirName() + " " + dockerConf.getJdkPath() + " \n");
        builder.append("COPY " + publishConf.getZookeeperDirName() + " " + dockerConf.getZookerPath() + " \n");
        builder.append("ADD startD.sh /opt/data/shell/startD.sh \n");
        for (AppModelVo appModelVo : appModelVos) {
            builder.append("ADD " + appModelVo.getFinalName() + " /opt/data/" + projectModelVo.getMainPath() + "/" + appModelVo.getMainPath() + "/" + appModelVo.getFinalName() + " \n");
        }
        builder.append("ENV JAVA_HOME=/usr/local/java \n");
        builder.append("ENV PATH=$JAVA_HOME/bin:$PATH \n");
        builder.append("ENV CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME /lib/tools.jar \n");
        builder.append("EXPOSE 8888 \n");
        builder.append("CMD [\"sh\", \"/opt/data/shell/startD.sh\"] \n");
        FileUtils.writeByteArrayToFile(dockerFile, builder.toString().getBytes());
    }

    private void createStartDSh(String taskId, ProjectModelVo projectModelVo, List<AppModelVo> appModelVos) throws Exception {
        //动态创建Dockerfile
        File startShFile = new File(publishConf.getWorkspace() + File.separator + taskId + File.separator + "dockertask" + File.separator + "startD.sh");
        StringBuilder builder = new StringBuilder();
        builder.append("# !/bin/bash \n");
        builder.append("mkdir " + dockerConf.getZookerDataPath() + " \n");
        builder.append("cd " + dockerConf.getZookerPath() + "bin \n");
        builder.append("sh zkServer.sh start \n");
        for (AppModelVo appModelVo : appModelVos) {
            builder.append("cd /opt/data/" + projectModelVo.getMainPath() + "/" + appModelVo.getMainPath() + " \n");
            builder.append("java -jar " + appModelVo.getFinalName() + ">/dev/null 2>&1 & \n");
        }
        FileUtils.writeByteArrayToFile(startShFile, builder.toString().getBytes());
    }

    @Async("taskBuildThreadPool")
    public void runBuildTask(String branch, String taskId, ProjectModelVo projectModelVo, List<AppModelVo> appModelVos) {
        try {
            publishLogModelMapper.insertLog(taskId, "");
            shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "download.sh", new String[]{taskId, publishConf.getWorkspace(), projectModelVo.getGitUrl(), branch, projectModelVo.getMainPath()}, "<end></end>");
            for (AppModelVo appModelVo : appModelVos) {
                shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "install.sh", new String[]{taskId, publishConf.getWorkspace(), projectModelVo.getMainPath(), appModelVo.getMainPath()}, "<end></end>");
            }
            shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "copyenv.sh", new String[]{taskId, publishConf.getWorkspace(), publishConf.getJdkPath(), publishConf.getZookerPath()}, "<end></end>");
            for (AppModelVo appModelVo : appModelVos) {
                String appPath = publishConf.getWorkspace() + File.separator + taskId + File.separator + projectModelVo.getMainPath() + File.separator + appModelVo.getMainPath() + File.separator + "target" + File.separator + appModelVo.getFinalName();
                shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "copyapp.sh", new String[]{taskId, publishConf.getWorkspace(), appPath}, "<end></end>");
            }
            createDockerFile(taskId, projectModelVo, appModelVos);
            createStartDSh(taskId, projectModelVo, appModelVos);
            //创建镜像
            shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "buildMirror.sh", new String[]{taskId, publishConf.getWorkspace(), "zhouxin"}, "<end></end>");
            buildTaskService.updateTaskStatus(taskId, PublishTaskStatusEnum.BUILD_FINISHED.getValue());
        } catch (Exception e) {
            log.error("runBuildTask error={}", e);
        }
    }

    @Async("taskBuildThreadPool")
    public void runBuildTaskNew(String taskId, List<BuildAppParam> buildAppParamList) {
        Map<Long, Boolean> initDownMap = new HashMap<>();
        try {
            publishLogModelMapper.insertLog(taskId, "");
            shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "copyenv.sh", new String[]{taskId, publishConf.getWorkspace(), publishConf.getJdkPath(), publishConf.getZookerPath()}, "<end></end>");
            for (BuildAppParam buildAppParam : buildAppParamList) {
                ProjectModel projectModel = projectModelMapper.findById(buildAppParam.getProjectId());
                if (initDownMap.get(projectModel.getId()) == null) {
                    shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "download.sh", new String[]{taskId, publishConf.getWorkspace(), projectModel.getGitUrl(), buildAppParam.getBranch(), projectModel.getMainPath()}, "<end></end>");
                    initDownMap.put(projectModel.getId(), true);
                }
                AppModel appModel = appModelMappe.findById(buildAppParam.getAppId());
                shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "install.sh", new String[]{taskId, publishConf.getWorkspace(), projectModel.getMainPath(), appModel.getMainPath()}, "<end></end>");
                String appPath = publishConf.getWorkspace() + File.separator + taskId + File.separator + projectModel.getMainPath() + File.separator + appModel.getMainPath() + File.separator + "target" + File.separator + appModel.getFinalName();
                shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "copyapp.sh", new String[]{taskId, publishConf.getWorkspace(), appPath}, "<end></end>");
            }
            createDockerFileNew(taskId, buildAppParamList);
            createStartDShNew(taskId, buildAppParamList);
            //创建镜像
            shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "buildMirror.sh", new String[]{taskId, publishConf.getWorkspace(), "zhouxin"}, "<end></end>");
            buildTaskService.updateTaskStatus(taskId, PublishTaskStatusEnum.BUILD_FINISHED.getValue());
        } catch (Exception e) {
            log.error("runBuildTask error={}", e);
        }
    }


    private void createDockerFileNew(String taskId, List<BuildAppParam> buildAppParamList) throws Exception {
        //动态创建Dockerfile
        File dockerFile = new File(publishConf.getWorkspace() + File.separator + taskId + File.separator + "dockertask" + File.separator + "Dockerfile");
        StringBuilder builder = new StringBuilder();
        builder.append("FROM " + dockerConf.getImgUbuntu() + " \n");
        builder.append("MAINTAINER zhouxin \n");
        builder.append("COPY " + publishConf.getJdkDirName() + " " + dockerConf.getJdkPath() + " \n");
        builder.append("COPY " + publishConf.getZookeeperDirName() + " " + dockerConf.getZookerPath() + " \n");
        builder.append("ADD startD.sh /opt/data/shell/startD.sh \n");
        for (BuildAppParam buildAppParam : buildAppParamList) {
            ProjectModel projectModel = projectModelMapper.findById(buildAppParam.getProjectId());
            AppModel appModel = appModelMappe.findById(buildAppParam.getAppId());
            builder.append("ADD " + appModel.getFinalName() + " /opt/data/" + projectModel.getMainPath() + "/" + appModel.getMainPath() + "/" + appModel.getFinalName() + " \n");
        }
        builder.append("ENV JAVA_HOME=/usr/local/java \n");
        builder.append("ENV PATH=$JAVA_HOME/bin:$PATH \n");
        builder.append("ENV CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar \n");
        List<AppModel> appModels = appModelMappe.findAll();
        for (AppModel appModel : appModels) {
            if (appModel.getAppPort() == null) {
                continue;
            }
            builder.append("EXPOSE " + appModel.getAppPort() + " \n");
        }
        builder.append("CMD [\"sh\", \"/opt/data/shell/startD.sh\"] \n");
        FileUtils.writeByteArrayToFile(dockerFile, builder.toString().getBytes());
    }

    private void createStartDShNew(String taskId, List<BuildAppParam> buildAppParamList) throws Exception {
        //动态创建Dockerfile
        File startShFile = new File(publishConf.getWorkspace() + File.separator + taskId + File.separator + "dockertask" + File.separator + "startD.sh");
        StringBuilder builder = new StringBuilder();
        builder.append("# !/bin/bash \n");
        builder.append("mkdir " + dockerConf.getZookerDataPath() + " \n");
        builder.append("cd " + dockerConf.getZookerPath() + "bin \n");
        builder.append("sh zkServer.sh start \n");
        for (BuildAppParam buildAppParam : buildAppParamList) {
            ProjectModel projectModel = projectModelMapper.findById(buildAppParam.getProjectId());
            AppModel appModel = appModelMappe.findById(buildAppParam.getAppId());
            builder.append("cd /opt/data/" + projectModel.getMainPath() + "/" + appModel.getMainPath() + " \n");
            builder.append("java -jar " + appModel.getFinalName() + ">/dev/null 2>&1 & \n");
            //builder.append("java -jar " + appModel.getFinalName() + " \n");
        }
        builder.append("while true \n");
        builder.append("do \n");
        builder.append("sleep 1 \n");
        builder.append("echo '<end></end>' \n");
        builder.append("done \n");

        FileUtils.writeByteArrayToFile(startShFile, builder.toString().getBytes());
    }

    @Async("taskBuildThreadPool")
    public void runDocker(String taskId) {
        try {
            List<PublishTaskModelVo> publishTaskModelVos = buildTaskService.findByTasksId(taskId);
            Map<Long, Boolean> temp = new HashMap<>();
            for (PublishTaskModelVo publishTaskModelVo : publishTaskModelVos) {
                temp.put(publishTaskModelVo.getPublishAppId(), true);
            }
            String portsInfo = "";
            int tempPort = 9999;
            List<AppModel> appModels = appModelMappe.findAll();
            for (int i = 0; i < appModels.size(); i++) {
                AppModel appModel = appModels.get(i);
                if (appModel.getAppPort() == null) {
                    continue;
                }
                tempPort = tempPort + 1;
                if (temp.containsKey(appModel.getId())) {
                    publishTaskMapper.updateDockerOutPort(taskId, appModel.getId(), Long.valueOf(tempPort));
                }
                portsInfo = portsInfo + " -p " + publishConf.getOutIp() + ":" + tempPort + ":" + appModel.getAppPort();
            }
            String str = shellUtil.execShellByParamReturn(taskId, publishConf.getShellRootPath() + "run.sh", new String[]{"zhouxin-" + taskId, portsInfo, taskId}, "<end></end>");
            log.info("---------------------------------------------------------------------------" + str);
            String containerId = str.split("<end></end>")[0].trim();
            publishTaskMapper.updateRqid(taskId, containerId);
        } catch (Exception e) {
            log.error("runDocker error={}", e);
        }

    }


    @Async("taskBuildThreadPool")
    public void runBuildTaskNewLeft(String taskId, List<BuildAppParam> buildAppParamList, String rqid) {
        Map<Long, Boolean> initDownMap = new HashMap<>();
        List<AppModel> finalNames = new ArrayList<>();
        try {
            for (BuildAppParam buildAppParam : buildAppParamList) {
                ProjectModel projectModel = projectModelMapper.findById(buildAppParam.getProjectId());
                if (initDownMap.get(projectModel.getId()) == null) {
                    shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "downloadleft.sh", new String[]{taskId, publishConf.getWorkspace(), projectModel.getGitUrl(), buildAppParam.getBranch(), projectModel.getMainPath()}, "<end></end>");
                    initDownMap.put(projectModel.getId(), true);
                }
                AppModel appModel = appModelMappe.findById(buildAppParam.getAppId());
                finalNames.add(appModel);
                shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "install.sh", new String[]{taskId, publishConf.getWorkspace(), projectModel.getMainPath(), appModel.getMainPath()}, "<end></end>");
                String appPath = publishConf.getWorkspace() + File.separator + taskId + File.separator + projectModel.getMainPath() + File.separator + appModel.getMainPath() + File.separator + "target" + File.separator + appModel.getFinalName();
                shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "copyapp.sh", new String[]{taskId, publishConf.getWorkspace(), appPath}, "<end></end>");
            }
            createC1sh(taskId, rqid, finalNames);
            createC2sh(taskId, rqid, finalNames);
            shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "runc1.sh", new String[]{taskId, publishConf.getWorkspace()}, "<end></end>");
            shellUtil.execShellByParam(taskId, publishConf.getShellRootPath() + "runc2.sh", new String[]{taskId, publishConf.getWorkspace(), rqid}, "<end></end>");
            log.info("finish----------------------");
        } catch (Exception e) {
            log.error("runBuildTaskNewLeft error={}", e);
        }
    }

    private void createC1sh(String taskId, String rqid, List<AppModel> finaNames) throws Exception {
        //动态创建Dockerfile
        File startShFile = new File(publishConf.getWorkspace() + File.separator + taskId + File.separator + "dockertask" + File.separator + "c1.sh");
        StringBuilder builder = new StringBuilder();
        builder.append("# !/bin/bash \n");
        builder.append("cd /opt/publishwork/" + taskId + "/dockertask \n");
        builder.append("docker cp c2.sh " + rqid + ":/opt/data/ \n");
        for (AppModel fn : finaNames) {
            builder.append("docker cp " + fn.getFinalName() + " " + rqid + ":/opt/data/ \n");
        }
        FileUtils.writeByteArrayToFile(startShFile, builder.toString().getBytes());
    }

    private void createC2sh(String taskId, String rqid, List<AppModel> finaNames) throws Exception {
        //动态创建Dockerfile
        File startShFile = new File(publishConf.getWorkspace() + File.separator + taskId + File.separator + "dockertask" + File.separator + "c2.sh");
        StringBuilder builder = new StringBuilder();
        builder.append("# !/bin/bash \n");
        for (AppModel appModel : finaNames) {
            ProjectModel projectModel = projectModelMapper.findById(appModel.getProjectId());
            builder.append("cd /opt/data/ \n");
            builder.append("mkdir " + projectModel.getMainPath() + " \n");
            builder.append("cd " + projectModel.getMainPath() + " \n");
            builder.append("mkdir " + appModel.getMainPath() + " \n");
            builder.append("cd /opt/data/ \n");
            builder.append("cp " + appModel.getFinalName() + " /opt/data/" + projectModel.getMainPath() + "/" + appModel.getMainPath() + " \n");
            builder.append("rm -rf " + appModel.getFinalName() + " \n");
            builder.append("cd /opt/data/" + projectModel.getMainPath() + "/" + appModel.getMainPath() + " \n");
            builder.append("java -jar " + appModel.getFinalName() + ">/dev/null 2>&1 &  \n");
        }
        builder.append("while true \n");
        builder.append("do \n");
        builder.append("sleep 1 \n");
        builder.append("echo '<end></end>' \n");
        builder.append("done \n");
        FileUtils.writeByteArrayToFile(startShFile, builder.toString().getBytes());


    }


}
