package com.zx.publish.publishcore.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.zx.publish.publishcore.mapper.AppModelMappe;
import com.zx.publish.publishcore.mapper.ProjectModelMapper;
import com.zx.publish.publishcore.mapper.PublishTaskMapper;
import com.zx.publish.publishcore.model.AppModel;
import com.zx.publish.publishcore.model.ProjectModel;
import com.zx.publish.publishcore.model.PublishTaskModel;
import com.zx.publish.publishcoreapi.ProjectService;
import com.zx.publish.publishcoreapi.vo.AppModelVo;
import com.zx.publish.publishcoreapi.vo.ProjectModelVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service(retries = 0)
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectModelMapper projectModelMapper;
    @Autowired
    private PublishTaskMapper publishTaskMapper;
    @Autowired
    private AppModelMappe appModelMappe;

    @Override
    public List<ProjectModelVo> findAllProjects() {
        List<ProjectModel> projectModels = projectModelMapper.findAll();
        List<ProjectModelVo> projectModelVos = new ArrayList<>();
        for (ProjectModel pm : projectModels) {
            ProjectModelVo projectModelVo = new ProjectModelVo();
            BeanUtils.copyProperties(pm, projectModelVo);
            projectModelVos.add(projectModelVo);
        }
        return projectModelVos;
    }

    @Override
    public ProjectModelVo findById(Long projectId) {
        ProjectModel projectModel = projectModelMapper.findById(projectId);
        ProjectModelVo projectModelVo = new ProjectModelVo();
        BeanUtils.copyProperties(projectModel, projectModelVo);
        return projectModelVo;
    }

    @Override
    public List<ProjectModelVo> findLeftProjects(String taskId) {
        Map<Long, Boolean> temp = new HashMap<>();
        List<PublishTaskModel> publishTaskModels = publishTaskMapper.findTasksByTaskId(taskId);
        for (PublishTaskModel publishTaskModel : publishTaskModels) {
            temp.put(publishTaskModel.getPublishAppId(), true);
        }
        List<ProjectModelVo> result = new ArrayList<>();
        List<ProjectModel> projectModels = projectModelMapper.findAll();
        for (ProjectModel pm : projectModels) {
            ProjectModelVo projectModelVo = new ProjectModelVo();
            BeanUtils.copyProperties(pm, projectModelVo);
            List<AppModelVo> appModelVoList = new ArrayList<>();
            List<AppModel> appModels = appModelMappe.findByProjectId(pm.getId());
            for (AppModel appModel : appModels) {
                if (!temp.containsKey(appModel.getId())) {
                    AppModelVo appModelVo = new AppModelVo();
                    BeanUtils.copyProperties(appModel, appModelVo);
                    appModelVoList.add(appModelVo);
                }
            }
            projectModelVo.setAppModelVoList(appModelVoList);
            result.add(projectModelVo);
        }
        return result;
    }
}
