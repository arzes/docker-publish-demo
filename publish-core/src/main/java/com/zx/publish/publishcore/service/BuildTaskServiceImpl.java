package com.zx.publish.publishcore.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.zx.publish.publishcore.mapper.PublishLogModelMapper;
import com.zx.publish.publishcore.mapper.PublishTaskMapper;
import com.zx.publish.publishcore.model.PublishLogModel;
import com.zx.publish.publishcore.model.PublishTaskModel;
import com.zx.publish.publishcoreapi.BuildTaskService;
import com.zx.publish.publishcoreapi.vo.PublishLogVo;
import com.zx.publish.publishcoreapi.vo.PublishTaskModelVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service(retries = 0)
public class BuildTaskServiceImpl implements BuildTaskService {
    @Autowired
    private PublishTaskMapper publishTaskMapper;
    @Autowired
    private PublishLogModelMapper publishLogModelMapper;

    @Override
    public void updateTaskStatus(String taskId, Integer taskStatus) {
        publishTaskMapper.updateTaskStatus(taskId, taskStatus);
    }

    @Override
    public PublishLogVo findByTaskId(String taskId) {
        PublishLogModel publishLogModel = publishLogModelMapper.findByTaskId(taskId);
        PublishLogVo publishLogVo = new PublishLogVo();
        BeanUtils.copyProperties(publishLogModel, publishLogVo);
        return publishLogVo;
    }

    @Override
    public List<PublishTaskModelVo> findByTasksId(String taskId) {
        List<PublishTaskModel> publishTaskModels = publishTaskMapper.findTasksByTaskId(taskId);
        List<PublishTaskModelVo> publishTaskModelVos = new ArrayList<>();
        for (PublishTaskModel publishTaskModel : publishTaskModels) {
            PublishTaskModelVo publishTaskModelVo = new PublishTaskModelVo();
            BeanUtils.copyProperties(publishTaskModel, publishTaskModelVo);
            publishTaskModelVos.add(publishTaskModelVo);
        }
        return publishTaskModelVos;
    }
}
