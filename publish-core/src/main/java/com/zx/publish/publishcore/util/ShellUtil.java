package com.zx.publish.publishcore.util;

import com.zx.publish.publishcore.mapper.PublishLogModelMapper;
import com.zx.publish.publishcore.model.PublishLogModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;

@Component
@Slf4j
public class ShellUtil {
    @Autowired
    private PublishLogModelMapper publishLogModelMapper;

    public void execShellByParam(String taskId, String filepath, String[] param, String endTag) throws Exception {
        CommandLine cmdLine = CommandLine.parse("sh " + filepath);
        cmdLine.addArguments(param, true);
        DefaultExecutor exec = new DefaultExecutor();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        exec.setStreamHandler(new PumpStreamHandler(baos));

        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        exec.execute(cmdLine, resultHandler);
        PublishLogModel publishLogModel = publishLogModelMapper.findByTaskId(taskId);
        while (true) {
            String temp = new String(baos.toString("UTF-8"));
            temp = temp.replace("\r\n", "</br>");
            temp = temp.replace("\r", "</br>");
            temp = temp.replace("\n", "</br>");
            boolean isend = false;
            if (temp.toString().indexOf(endTag) != -1) {
                publishLogModelMapper.updateLog(taskId, publishLogModel.getLog() + temp);
                publishLogModel.setLog(publishLogModel.getLog() + temp);
                isend = true;
            }
            log.info(temp);
            if (isend) {
                break;
            }
            Thread.sleep(1000);
        }
        exec.setExitValue(1);
    }

    public String execShellByParamReturn(String taskId, String filepath, String[] param, String endTag) throws Exception {
        CommandLine cmdLine = CommandLine.parse("sh " + filepath);
        cmdLine.addArguments(param, true);
        DefaultExecutor exec = new DefaultExecutor();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        exec.setStreamHandler(new PumpStreamHandler(baos));
        StringBuilder builder = new StringBuilder();
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        exec.execute(cmdLine, resultHandler);
        while (true) {
            String temp = new String(baos.toString("UTF-8"));
            boolean isend = false;
            if (temp.toString().indexOf(endTag) != -1) {
                isend = true;
            }
            log.info(temp);
            if (isend) {
                builder.append(temp);
                break;
            }
            Thread.sleep(1000);
        }
        exec.setExitValue(1);
        return builder.toString();
    }
}
