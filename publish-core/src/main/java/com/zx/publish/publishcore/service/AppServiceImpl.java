package com.zx.publish.publishcore.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.zx.publish.publishcore.mapper.AppModelMappe;
import com.zx.publish.publishcore.model.AppModel;
import com.zx.publish.publishcoreapi.AppService;
import com.zx.publish.publishcoreapi.vo.AppModelVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service(retries = 0)
public class AppServiceImpl implements AppService {
    @Autowired
    private AppModelMappe appModelMappe;

    @Override
    public List<AppModelVo> findAppByProject(Long projectId) {
        List<AppModelVo> appModelVos = new ArrayList<>();
        List<AppModel> appModels = appModelMappe.findByProjectId(projectId);
        for (AppModel am : appModels) {
            AppModelVo appModelVo = new AppModelVo();
            BeanUtils.copyProperties(am, appModelVo);
            appModelVos.add(appModelVo);
        }
        return appModelVos;
    }

    @Override
    public AppModelVo findAppById(Long appId) {
        AppModel appModel = appModelMappe.findById(appId);
        AppModelVo appModelVo = new AppModelVo();
        BeanUtils.copyProperties(appModel, appModelVo);
        return appModelVo;
    }
}
