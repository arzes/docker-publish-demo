package com.zx.publish.publishcore.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.zx.publish.publishcore.mapper.PublishTaskMapper;
import com.zx.publish.publishcore.model.PublishTaskModel;
import com.zx.publish.publishcoreapi.AppService;
import com.zx.publish.publishcoreapi.BuildService;
import com.zx.publish.publishcoreapi.ProjectService;
import com.zx.publish.publishcoreapi.enums.PublishTaskStatusEnum;
import com.zx.publish.publishcoreapi.params.BuildAppParam;
import com.zx.publish.publishcoreapi.params.TaskParam;
import com.zx.publish.publishcoreapi.vo.AppModelVo;
import com.zx.publish.publishcoreapi.vo.ProjectModelVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@Slf4j
@Service(retries = -1, timeout = 1000000)
public class BuildServiceImpl implements BuildService {

    @Autowired
    private PublishTaskMapper publishTaskMapper;
    @Autowired
    private AppService appService;
    @Autowired
    private BuildPoolService buildPoolService;
    @Autowired
    private ProjectService projectService;

    @Override
    public void startBuild(TaskParam taskParam) {
        ProjectModelVo projectModelVo = projectService.findById(taskParam.getPublishProjectId());
        List<AppModelVo> appModelVos = appService.findAppByProject(taskParam.getPublishProjectId());
        for (AppModelVo appModelVo : appModelVos) {
            PublishTaskModel publishTaskModel = new PublishTaskModel();
            publishTaskModel.setPublishTaskId(taskParam.getTaskUUID());
            publishTaskModel.setPublishProjectId(taskParam.getPublishProjectId());
            publishTaskModel.setPublishAppId(appModelVo.getId());
            publishTaskModel.setBranch(taskParam.getBranch());
            publishTaskModel.setCreatedAt(new Date());
            publishTaskModel.setUpdatedAt(new Date());
            publishTaskModel.setTaskStatus(PublishTaskStatusEnum.BUILDING.getValue());
            publishTaskMapper.createPublishTask(publishTaskModel);
        }
        buildPoolService.runBuildTask(taskParam.getBranch(), taskParam.getTaskUUID(), projectModelVo, appModelVos);
    }

    @Override
    public void startBuildNew(String taskId, List<BuildAppParam> buildAppParamList) {
        log.info("startBuildNew--------------------");
        for (BuildAppParam buildAppParam : buildAppParamList) {
            ProjectModelVo projectModelVo = projectService.findById(buildAppParam.getProjectId());
            AppModelVo appModelVo = appService.findAppById(buildAppParam.getAppId());
            PublishTaskModel publishTaskModel = new PublishTaskModel();
            publishTaskModel.setPublishTaskId(taskId);
            publishTaskModel.setPublishProjectId(buildAppParam.getProjectId());
            publishTaskModel.setPublishAppId(appModelVo.getId());
            publishTaskModel.setBranch(buildAppParam.getBranch());
            publishTaskModel.setCreatedAt(new Date());
            publishTaskModel.setUpdatedAt(new Date());
            publishTaskModel.setTaskStatus(PublishTaskStatusEnum.BUILDING.getValue());
            publishTaskMapper.createPublishTask(publishTaskModel);
        }
        buildPoolService.runBuildTaskNew(taskId, buildAppParamList);
    }

    @Override
    public List<String> findBuildFinishedTask() {
        return publishTaskMapper.findBuildFinishedTaskId();
    }

    @Override
    public void run(String taskId) {
        buildPoolService.runDocker(taskId);
    }

    @Override
    public void startBuildNewLeft(String taskId, List<BuildAppParam> buildAppParamList) {
        PublishTaskModel publishTaskModelq = publishTaskMapper.findTasksByTaskId(taskId).get(0);
        for (BuildAppParam buildAppParam : buildAppParamList) {
            ProjectModelVo projectModelVo = projectService.findById(buildAppParam.getProjectId());
            AppModelVo appModelVo = appService.findAppById(buildAppParam.getAppId());
            PublishTaskModel publishTaskModel = new PublishTaskModel();
            publishTaskModel.setPublishTaskId(taskId);
            publishTaskModel.setPublishProjectId(buildAppParam.getProjectId());
            publishTaskModel.setPublishAppId(appModelVo.getId());
            publishTaskModel.setBranch(buildAppParam.getBranch());
            publishTaskModel.setCreatedAt(new Date());
            publishTaskModel.setUpdatedAt(new Date());
            publishTaskModel.setTaskStatus(PublishTaskStatusEnum.BUILD_FINISHED.getValue());
            publishTaskModel.setRqid(publishTaskModelq.getRqid());
            publishTaskMapper.createPublishTask2(publishTaskModel);
        }
        String rqid=publishTaskModelq.getRqid().substring(0,8);
        buildPoolService.runBuildTaskNewLeft(taskId, buildAppParamList,rqid);
    }
}
