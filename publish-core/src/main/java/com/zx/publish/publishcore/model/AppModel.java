package com.zx.publish.publishcore.model;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class AppModel {
    private Long id;
    private String appId;
    private String appName;
    private Long projectId;
    private Date createdAt;
    private Date updatedAt;
    private String mainPath;
    private String finalName;
    private Long appPort;
}
