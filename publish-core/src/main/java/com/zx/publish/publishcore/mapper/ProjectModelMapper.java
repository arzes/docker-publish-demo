package com.zx.publish.publishcore.mapper;

import com.zx.publish.publishcore.model.ProjectModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProjectModelMapper {
    public List<ProjectModel> findAll();

    public ProjectModel findById(@Param(value = "projectId") Long projectId);
}
