package com.zx.publish.publishcore.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PublishLogModel {
    private String publishTaskId;
    private String log;
}
