package com.zx.publish.publishcore.mapper;

import com.zx.publish.publishcore.model.PublishLogModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PublishLogModelMapper {
    public void insertLog(@Param(value = "taskId") String taskId, @Param(value = "log") String log);

    public void updateLog(@Param(value = "taskId") String taskId, @Param(value = "log") String log);

    public PublishLogModel findByTaskId(@Param(value = "taskId") String taskId);
}
