package com.zx.publish.publishcore.mapper;

import com.zx.publish.publishcore.model.PublishTaskModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PublishTaskMapper {
    public void createPublishTask(PublishTaskModel publishTaskModel);
    public void createPublishTask2(PublishTaskModel publishTaskModel);

    public void updateTaskStatus(@Param(value = "taskId") String taskId, @Param(value = "taskStatus") Integer taskStatus);

    public List<String> findBuildFinishedTaskId();

    public List<PublishTaskModel> findTasksByTaskId(@Param(value = "taskId") String taskId);

    public void updateRqid(@Param(value = "taskId") String taskId, @Param(value = "rqid") String rqid);

    public void updateDockerOutPort(@Param(value = "taskId") String taskId,@Param(value = "appId") Long appId, @Param(value = "dockerOutPort") Long dockerOutPort);
}
