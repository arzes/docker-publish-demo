package com.zx.publish.publishcore.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
@ToString
public class PublishConf {
    @Value("${publishconf.workspace}")
    private String workspace;
    @Value("${publishconf.shellpath}")
    private String shellRootPath;
    @Value("${publishconf.jdkPath}")
    private String jdkPath;
    @Value("${publishconf.zookeeperPath}")
    private String zookerPath;
    @Value("${publishconf.jdkDirName}")
    private String jdkDirName;
    @Value("${publishconf.zookeeperDirName}")
    private String zookeeperDirName;
    @Value("${publishconf.outIp}")
    private String outIp;

}
