package com.zx.publish.publishcore.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
@ToString
public class DockerConf {
    @Value("${dockerconf.ubuntu}")
    private String imgUbuntu;

    @Value("${dockerconf.jdkPath}")
    private String jdkPath;
    @Value("${dockerconf.zookerPath}")
    private String zookerPath;
    @Value("${dockerconf.zookeprDataPath}")
    private String zookerDataPath;

}
