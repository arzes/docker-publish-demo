package com.zx.publish.publishcore.model;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class ProjectModel {
    private Long id;
    private String projectName;
    private Date createdAt;
    private Date updatedAt;
    private String gitUrl;
    private String mainPath;
}
