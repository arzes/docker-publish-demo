package com.zx.publish.publishcore.mapper;

import com.zx.publish.publishcore.model.AppModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AppModelMappe {
    public List<AppModel> findByProjectId(@Param(value = "projectId") Long projectId);

    public AppModel findById(@Param(value = "appId") Long appId);

    public List<AppModel> findAll();
}
