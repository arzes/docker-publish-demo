package com.zx.publish.publishdemo.service;

import com.zx.publish.publishdemo.vo.GitBranchVo;
import com.zx.publish.publishdemo.vo.GitProjectVo;

import java.util.List;

public interface GitService {
    public GitProjectVo findByProjectGitUrl(String projectGitUrl) throws Exception;

    public List<GitBranchVo> findBranchByProjectId(Long projectId) throws Exception;
}
