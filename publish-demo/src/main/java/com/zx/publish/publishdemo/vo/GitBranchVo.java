package com.zx.publish.publishdemo.vo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class GitBranchVo {
    private String name;
}
