package com.zx.publish.publishdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PublishDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PublishDemoApplication.class, args);
    }
}
