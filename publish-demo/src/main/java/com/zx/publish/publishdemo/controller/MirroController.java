package com.zx.publish.publishdemo.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.zx.publish.publishcoreapi.AppService;
import com.zx.publish.publishcoreapi.BuildService;
import com.zx.publish.publishcoreapi.BuildTaskService;
import com.zx.publish.publishcoreapi.ProjectService;
import com.zx.publish.publishcoreapi.params.BuildAppParam;
import com.zx.publish.publishcoreapi.params.TaskParam;
import com.zx.publish.publishcoreapi.vo.AppModelVo;
import com.zx.publish.publishcoreapi.vo.ProjectModelVo;
import com.zx.publish.publishdemo.service.GitService;
import com.zx.publish.publishdemo.util.ShellUtil;
import com.zx.publish.publishdemo.vo.GitBranchVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@Slf4j
@RequestMapping(value = "/mirror/")
public class MirroController {

    @Autowired
    private ShellUtil shellUtil;
    @Reference(retries = 0)
    private ProjectService projectService;
    @Reference(retries = 0)
    private AppService appService;
    @Autowired
    private GitService gitService;
    @Reference(retries = -1, timeout = 1000000)
    private BuildService buildService;
    @Reference(retries = -1, timeout = 1000000)
    private BuildTaskService buildTaskService;

    @RequestMapping(value = "build", method = RequestMethod.GET)
    public String build(Model model) {
        model.addAttribute("projects", projectService.findAllProjects());
        return "mirror/mirrorBuild";
    }

    @RequestMapping(value = "loadApps", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public List<AppModelVo> loadApps(@RequestParam(value = "projectId", required = true) Long projectId) throws Exception {
        return appService.findAppByProject(projectId);
    }

    @RequestMapping(value = "loadBranchs", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public List<GitBranchVo> loadBranchs(@RequestParam(value = "projectId", required = true) Long projectId) throws Exception {
        List<GitBranchVo> gitBranchVos = gitService.findBranchByProjectId(projectId);
        return gitBranchVos;
    }

    @RequestMapping(value = "createTask", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String createTask(TaskParam taskParam) throws Exception {
        UUID uuid = UUID.randomUUID();
        taskParam.setTaskUUID(uuid.toString());
        buildService.startBuild(taskParam);
        return uuid.toString();
    }

    @RequestMapping(value = "log", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String log(@RequestParam(value = "taskId", required = true) String taskId) throws Exception {
        return buildTaskService.findByTaskId(taskId).getLog();
    }

    @RequestMapping(value = "buildNew", method = RequestMethod.GET)
    public String buildNew(Model model) throws Exception {
        List<ProjectModelVo> projectModelVos = projectService.findAllProjects();
        for (ProjectModelVo projectModelVo : projectModelVos) {
            List<AppModelVo> appModelVos = appService.findAppByProject(projectModelVo.getId());
            projectModelVo.setAppModelVoList(appModelVos);
            List<GitBranchVo> gitBranchVos = gitService.findBranchByProjectId(projectModelVo.getId());
            List<String> branchs = new ArrayList<>();
            for (GitBranchVo gitBranchVo : gitBranchVos) {
                branchs.add(gitBranchVo.getName());
            }
            projectModelVo.setGitBranchVos(branchs);
        }
        model.addAttribute("projects", projectModelVos);
        return "mirror/mirrorBuildNew";
    }

    @RequestMapping(value = "createTaskNew", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String createTaskNew(@RequestParam(value = "buildAppParamList", required = true) String buildAppParamList) throws Exception {
        UUID uuid = UUID.randomUUID();
        List<BuildAppParam> buildAppParams = JSONObject.parseArray(buildAppParamList, BuildAppParam.class);
        buildService.startBuildNew(uuid.toString(), buildAppParams);
        log.info("createTaskNew--------------------");
        return uuid.toString();
    }

    @RequestMapping(value = "runlist", method = RequestMethod.GET)
    public String runlist(Model model) {
        model.addAttribute("taskIds", buildService.findBuildFinishedTask());
        return "mirror/runlist";
    }

    @RequestMapping(value = "run", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String run(@RequestParam(value = "taskId", required = true) String taskId) throws Exception {
        buildService.run(taskId);
        return "success";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(Model model, @RequestParam(value = "taskId", required = true) String taskId) throws Exception {
        List<ProjectModelVo> projectModelVos = projectService.findLeftProjects(taskId);
        for (ProjectModelVo projectModelVo : projectModelVos) {
            List<GitBranchVo> gitBranchVos = gitService.findBranchByProjectId(projectModelVo.getId());
            List<String> branchs = new ArrayList<>();
            for (GitBranchVo gitBranchVo : gitBranchVos) {
                branchs.add(gitBranchVo.getName());
            }
            projectModelVo.setGitBranchVos(branchs);
        }
        model.addAttribute("projects", projectModelVos);
        model.addAttribute("taskId", taskId);
        return "mirror/add";
    }

    @RequestMapping(value = "createTaskNewLeft", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String createTaskNewLeft(@RequestParam(value = "taskId", required = true) String taskId, @RequestParam(value = "buildAppParamList", required = true) String buildAppParamList) throws Exception {

        log.info("createTaskNewLeft--------------------");
        List<BuildAppParam> buildAppParams = JSONObject.parseArray(buildAppParamList, BuildAppParam.class);
        buildService.startBuildNewLeft(taskId, buildAppParams);
        return "success";
    }
}
