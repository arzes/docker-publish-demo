package com.zx.publish.publishdemo.util;

import com.zx.publish.publishdemo.config.GitConfig;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class HttpUtil {
    private Executor executor;

    @Value("${httpMaxSize}")
    private Integer httpMaxSize;
    @Autowired
    private GitConfig gitConfig;

    @PostConstruct
    public void init() {
        executor = Executor.newInstance(HttpClientBuilder.create().setMaxConnTotal(httpMaxSize).build());
    }

    public String getHttp(String url) throws IOException {
        String response = executor.execute(Request.Get(url).connectTimeout(10000).socketTimeout(10000)).returnContent().asString();
        return response;
    }

    public String getHttp(String url, String head, String value) throws IOException {
        String response = executor.execute(Request.Get(url).addHeader(head, value).connectTimeout(10000).socketTimeout(10000)).returnContent().asString();
        return response;
    }
}
