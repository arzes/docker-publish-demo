package com.zx.publish.publishdemo.util;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;

@Component
public class ShellUtil {

    public String execShellByParam(String filepath, String[] param, String endTag) throws Exception {
        StringBuilder builder = new StringBuilder();
        CommandLine cmdLine = CommandLine.parse("sh " + filepath);
        cmdLine.addArguments(param, true);
        DefaultExecutor exec = new DefaultExecutor();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        exec.setStreamHandler(new PumpStreamHandler(baos));

        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        exec.execute(cmdLine, resultHandler);

        while (true) {
            String temp = new String(baos.toString("UTF-8"));
            temp = temp.replace("\r\n", "</br>");
            temp = temp.replace("\r", "</br>");
            temp = temp.replace("\n", "</br>");
            boolean isend = false;
            if (temp.toString().indexOf(endTag) != -1) {
                builder.append(temp);
                isend = true;
            }
            if (isend) {
                break;
            }
            Thread.sleep(1000);
        }
        exec.setExitValue(1);
        return builder.toString();
    }
}
