package com.zx.publish.publishdemo.vo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class GitProjectVo {
    private Long id;
    private String http_url_to_repo;
}
