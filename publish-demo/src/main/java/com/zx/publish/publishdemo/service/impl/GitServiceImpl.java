package com.zx.publish.publishdemo.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.zx.publish.publishcoreapi.ProjectService;
import com.zx.publish.publishcoreapi.vo.ProjectModelVo;
import com.zx.publish.publishdemo.config.GitConfig;
import com.zx.publish.publishdemo.service.GitService;
import com.zx.publish.publishdemo.util.HttpUtil;
import com.zx.publish.publishdemo.vo.GitBranchVo;
import com.zx.publish.publishdemo.vo.GitProjectVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class GitServiceImpl implements GitService {
    @Autowired
    private HttpUtil httpUtil;
    @Autowired
    private GitConfig gitConfig;
    @Reference
    private ProjectService projectService;

    @Value("${git.userPass}")
    private String gitUserPass;


    @Override
    public GitProjectVo findByProjectGitUrl(String projectGitUrl) throws Exception {
        GitProjectVo gitProjectVo = null;
        String jsonstr = httpUtil.getHttp(gitConfig.getProjectsUrl() + gitConfig.getToken());
        List<GitProjectVo> gitProjectVoList = JSONObject.parseArray(jsonstr, GitProjectVo.class);
        for (GitProjectVo gv : gitProjectVoList) {
            String trimProjectUrl = projectGitUrl.replace(gitUserPass, "");
            if (gv.getHttp_url_to_repo().equals(trimProjectUrl)) {
                gitProjectVo = gv;
                break;
            }
        }

        return gitProjectVo;
    }

    @Override
    public List<GitBranchVo> findBranchByProjectId(Long projectId) throws Exception {
        List<GitBranchVo> gitBranchVos = null;
        ProjectModelVo projectModelVo = projectService.findById(projectId);
        if (projectModelVo == null) {
            return null;
        }
        GitProjectVo gitProjectVo = findByProjectGitUrl(projectModelVo.getGitUrl());
        if (gitProjectVo != null) {
            String jsonStr = httpUtil.getHttp(gitConfig.getBranchUrl().replace("@value@", gitProjectVo.getId() + ""), "PRIVATE-TOKEN", gitConfig.getToken());
            gitBranchVos = JSONObject.parseArray(jsonStr, GitBranchVo.class);
        }
        return gitBranchVos;
    }
}
