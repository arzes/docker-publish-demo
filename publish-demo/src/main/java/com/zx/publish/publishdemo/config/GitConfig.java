package com.zx.publish.publishdemo.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
@ToString
public class GitConfig {
    @Value("${git.token}")
    private String token;
    @Value("${git.projectsUrl}")
    private String projectsUrl;
    @Value("${git.branchUrl}")
    private String branchUrl;

}
