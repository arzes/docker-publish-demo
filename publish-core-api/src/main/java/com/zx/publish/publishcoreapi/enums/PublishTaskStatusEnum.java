package com.zx.publish.publishcoreapi.enums;

public enum PublishTaskStatusEnum {
    BUILDING(0),
    BUILD_FINISHED(1);
    private Integer value;

    PublishTaskStatusEnum(Integer v) {
        this.value = v;
    }

    public Integer getValue() {
        return value;
    }
}
