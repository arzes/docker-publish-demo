package com.zx.publish.publishcoreapi;

import com.zx.publish.publishcoreapi.vo.PublishLogVo;
import com.zx.publish.publishcoreapi.vo.PublishTaskModelVo;

import java.util.List;

public interface BuildTaskService {
    public void updateTaskStatus(String taskId, Integer taskStatus);

    public PublishLogVo findByTaskId(String taskId);

    public List<PublishTaskModelVo> findByTasksId(String taskId);
}
