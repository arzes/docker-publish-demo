package com.zx.publish.publishcoreapi.params;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class TaskParam implements Serializable {
    private Long publishProjectId;
    private String branch;
    private String taskUUID;
}
