package com.zx.publish.publishcoreapi.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class AppModelVo implements Serializable{
    private Long id;
    private String appId;
    private String appName;
    private Long projectId;
    private Date createdAt;
    private Date updatedAt;
    private String mainPath;
    private String finalName;
    private Long appPort;
}
