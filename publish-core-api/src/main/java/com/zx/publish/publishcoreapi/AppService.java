package com.zx.publish.publishcoreapi;

import com.zx.publish.publishcoreapi.vo.AppModelVo;

import java.util.List;

public interface AppService {
    public List<AppModelVo> findAppByProject(Long projectId);

    public AppModelVo findAppById(Long appId);
}
