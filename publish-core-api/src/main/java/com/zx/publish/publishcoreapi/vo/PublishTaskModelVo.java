package com.zx.publish.publishcoreapi.vo;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class PublishTaskModelVo {
    private Long id;
    private String publishTaskId;
    private Long publishProjectId;
    private Long publishAppId;
    private Date createdAt;
    private Date updatedAt;
    private String branch;
    private Integer taskStatus;
    private String rqid;
    private Long dockerOutPort;
}
