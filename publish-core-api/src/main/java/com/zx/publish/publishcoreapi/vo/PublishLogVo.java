package com.zx.publish.publishcoreapi.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class PublishLogVo implements Serializable {
    private String publishTaskId;
    private String log;
}
