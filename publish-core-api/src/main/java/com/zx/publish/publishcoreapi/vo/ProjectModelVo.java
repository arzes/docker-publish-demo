package com.zx.publish.publishcoreapi.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@ToString
public class ProjectModelVo implements Serializable {
    private Long id;
    private String projectName;
    private Date createdAt;
    private Date updatedAt;
    private String gitUrl;
    private String mainPath;
    private List<AppModelVo> appModelVoList;
    private List<String> gitBranchVos;
}
