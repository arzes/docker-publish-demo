package com.zx.publish.publishcoreapi;

import com.zx.publish.publishcoreapi.params.BuildAppParam;
import com.zx.publish.publishcoreapi.params.TaskParam;

import java.util.List;

public interface BuildService {
    public void startBuild(TaskParam taskParam);

    public void startBuildNew(String taskId, List<BuildAppParam> buildAppParamList);

    public List<String> findBuildFinishedTask();

    public void run(String taskId);

    public void startBuildNewLeft(String taskId, List<BuildAppParam> buildAppParamList);
}
