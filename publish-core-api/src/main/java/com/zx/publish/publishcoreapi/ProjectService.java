package com.zx.publish.publishcoreapi;


import com.zx.publish.publishcoreapi.vo.ProjectModelVo;

import java.util.List;

public interface ProjectService {
    public List<ProjectModelVo> findAllProjects();

    public ProjectModelVo findById(Long projectId);

    public List<ProjectModelVo> findLeftProjects(String taskId);
}
