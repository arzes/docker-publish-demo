package com.zx.publish.publishcoreapi.params;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class BuildAppParam implements Serializable {
    private Long projectId;
    private Long appId;
    private String branch;
}
